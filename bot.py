#!/usr/bin/python3 -u

import sys
import re
#import signal
import random

random.seed()

users = {}
botId = random.randint(1,1000)

def whois(colour, line):
    """Usage: 'i am [name]' or 'whois'."""
    global users
    if line.lower().startswith("i am "):
        users[colour] = line[5:]
        return ["Got it, " + users[colour]]
    if line.lower().strip() == "whois":
        userlist = []
        for colour in users:
            userlist.append("User: <span style='color:#" + colour + "'>" + users[colour] + "</span>")
        if userlist:
            return userlist
        else:
            return ["<i>No registered users.</i>"]
    return []

def choose(colour, line):
    """Usage: choose option_a or option_b - pick one at random, works for as many options as you like, 'or' separates options"""
    if line.lower().startswith("choose "):
        choices = line[7:].split(" or ")
        choices = [c for c in choices if c != "or"]
        return [choices[random.randint(0,len(choices)-1)]]
    return []

def yell(colour, line):
    """Usage: yell [some text]"""
    if line.lower().startswith("yell "):
        return ["<h1>" + line[5:] + "</h1>"]
    return []

def link(colour, line):
    """Usage: link some text with spaces http://example.com"""
    if line.lower().startswith("link "):
        line = line[5:].strip()
        end = line.rfind(" ")
        url = line[end+1:]
        title = line[:end] if end != -1 else line
        return ["Linkified: <a href='" + url + "' target='_blank'>" + title + "</a>"]
    return []

def image(colour, line):
    """Usage: image http://example.com/image.jpg"""
    if line.lower().startswith("image "):
        url = line[6:]
        return ["<a href='" + url + "' target='_blank'><img src='" + url + "' style='max-width:400px'></a>"]
    return []

def ping(colour, line):
    """Usage: ping [freeform text here]"""
    if line.lower().startswith("ping "):
        return ["pong " + line[5:]]
    return []

def alive(colour, line):
    """Usage: alive?"""
    if line.lower().startswith("alive?"):
        return ["I'm here! (id: " + str(botId) + ")"]
    return []

def quit(colour, line):
    """Usage: quit [botid]"""
    if line.lower().startswith("quit "):
        if line[5:] == str(botId):
            print("Bot says bye!")
            sys.exit(0);
    return []

def help(colour, line):
    """Usage: help - lists topic | help [topic] - this usage information"""
    if line.lower().startswith("help"):
        topic = line[5:].strip()
        if topic:
            for m in modules:
                if m.__name__ == topic:
                    return [m.__name__ + ": <i>" + m.__doc__ + "</i>"]
        else:
            return ["Modules: " + ", ".join([m.__name__ for m in modules])]
    return []

modules = [help, yell, whois, image, ping, link, alive, choose, quit]

#def signal_term_handler(signal, frame):
#    print("Bot says Bye!")
#    sys.exit(0)
#signal.signal(signal.SIGTERM, signal_term_handler)

print("Bot says hi! (id = " + str(botId) + ")", flush=True)
try:
    while True:
        line = sys.stdin.readline()
        separator = line.find(": ")
        colour = line[:separator]
        message = line[separator + 2:]
        if message.startswith("@bot "):
            responses = []
            for m in modules:
                responses.extend(m(colour, message[5:-1]))
            for r in responses:
                print(r, flush=True)
except KeyboardInterrupt:
    print("Bot says Bye!")
    sys.exit()

